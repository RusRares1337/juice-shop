# Using OWASP application which allow users to explore and understand a wide range of web application vulnerabilities, including SQL injection, XSS, CSRF,SSRF and more.
**Using technologies like GitLab CI/CD, Kubernetes, Docker, Terraform, Linux, Python and AWS from a Security perspective.**

## Learned: 
- OWASP Top 10 Application Security
- Shift Left Security; concepts and tools for SCA, SAST, DAST and OSS, how to apply these concepts and how to
work with these tools
- Security Risks and what need to be secured
- Security attacks (SQL injection, Path traversal, XSS, CSRF, SSRF)
- DevSecOps concepts and tools (different security types, what to automate and how)

## Application Security
- Secret scanning using GitLeaks to scan source code and detect hard-coded secrets
- SAST using njsscan and Semgrep to analyze code for security vulnerabilities
- SCA to find any third-party security vulnerabilities
- How to generate reports of each scanning tool
- How to use DefectDojo
- Upload and find remediation of security issues
- How to handle false positives
- Why and how to minimize false positives
- Fix security issues in the code
- CD part

## Cloud Security
- AWS access management (IAM - user,groups,policies,roles)
- Secure deployment from CI/CD
- DAST (concepts/tools)
- IaC
- GitOps
- Logging and Monitoring (CloudTrail and CloudWatch)

## Kubernetes Security
- Access management to K8s cluster
- RBAC, service accounts, roles and rolebindings
- Integrations with AWS IAM
- Pod security
- Control traffic on Pod level
- Service Mesh
- Ingress controllers
- Load balancers
- Secrets management (using hashicorp vault)
- Scan k8s misconfigurations
- Policy as code

## Compliance as code
- Compliance as Code concepts
- tools
- CIS Benchmarks